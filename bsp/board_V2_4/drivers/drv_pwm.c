/**
  *### Copyright (c) 2018, EDD-STC Development Team (eddstc1984@gmail.com). ######
  *### SPDX-License-Identifier: Apache-2.0 ######
  ********************************************************************************************
  * @file drv_pwm.c
  * @version 0.1.0 
  * @brief none
  * @details none
  * @warning none
  * @bug none  
  ********************************************************************************************
  *###Change Logs:
  *     Date        |   Author      |   Notes
  * ----------------|---------------|---------------------------------------------------------
  *     2019-2-10   |   WS          |   the first version
***/


/* Includes --------------------------------------------------------------------------------*/
#include "drv_pwm.h"
/** @addtogroup STC_BSP
  * @{
  */
/* Private typedef -------------------------------------------------------------------------*/
/* Private define --------------------------------------------------------------------------*/
/* Private macro ---------------------------------------------------------------------------*/
/* Private variables -----------------------------------------------------------------------*/
/* Private function prototypes -------------------------------------------------------------*/
/* Exported functions ----------------------------------------------------------------------*/
/**
  * @brief initialize PWM
  * @param[in] per: System cycle 
                    0x00-sysclk/12  0x02-sysclk/2  0x04-sysclk   0x06-sysclk
                    0x08-sysclk/1   0x0a-sysclk/4  0x0c-sysclk/6  0x0e-sysclk/8
  * @bug no timeout mechanism, may fall into an infinite loop
  */
void pwm_init(uint16_t per)
{
    CCON = 0;                      ///<Initial PCA control register///<Clear CF flag
    CL = 0;                        ///<PCA timer stop running
    CH = 0;                        ///<Clear all module interrupt flag
                                   ///<Reset PCA base timer    
    CMOD = per;                    ///<Set PCA timer clock source as Fosc/2
                                   ///<Disable PCA timer overflow interrupt      
    CCAPM0 = 0X42;                 ///<PCA module-0 work in 8-bit PWM mode and no PCA interrupt     
    CR = 1;                        ///<PCA timer start run          
    
}
/**
  * @brief Set duty cycle
  * @param[in] fre: duty cycle
                    If you want to get 50% duty cycle
                    fre =int( 256 * 50%)
  * @bug no timeout mechanism, may fall into an infinite loop
  */
void pwm_set(uint16_t fre)
{
    CCAP0H = CCAP0L = fre;            //PWM0 port output  cycle square wave
}    
  
/**
  * @}
  */
/************************ (C) COPYRIGHT WS *****END OF FILE*********************************/

