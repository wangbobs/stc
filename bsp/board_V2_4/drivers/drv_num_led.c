/**
  *### Copyright (c) 2018, EDD-STC Development Team (eddstc1984@gmail.com). ######
  *### SPDX-License-Identifier: Apache-2.0 ######
  ********************************************************************************************
  * @file drv_num_led.c
  * @version 0.1.0 
  * @brief none
  * @details none
  * @warning none
  * @bug none  
  ********************************************************************************************
  *###Change Logs:
  *     Date        |   Author      |   Notes
  * ----------------|---------------|---------------------------------------------------------
  *     2019-1-22   |   WS          |   the first version
***/


/* Includes --------------------------------------------------------------------------------*/
#include "drv_num_led.h"
/** @addtogroup STC_BSP
  * @{
  */
/* Private typedef -------------------------------------------------------------------------*/        
/* Private define --------------------------------------------------------------------------*/
#define P0_PORT P0
#define P2_PORT P2
/* Private macro ---------------------------------------------------------------------------*/
/* Private variables -----------------------------------------------------------------------*/
code uint8_t NUM_ARRAY[] = {0xC0, 0XF9, 0XA4, 0XB0, 0X99, 0x92, 0X82, 0XF8, 0X80, 0X90,0xff};
volatile uint8_t NUM_BUFF[7];
sbit ENABLE_NUMLED_RB = P1^1;
/* Private function prototypes -------------------------------------------------------------*/
/* Exported functions ----------------------------------------------------------------------*/
static uint32_t pow(int16_t multiplier_x,int16_t multiplier_y);
/**
  * @brief Initialize NUM_LED
  * @bug 
  */
void numled_init(void)
{
    uint8_t i;
    ENABLE_NUMLED_RB = 0;
    P0_PORT = 0xff;
    for (i = 0; i < 7; i++)
    {
        NUM_BUFF[i] = 0xff;
    }
    
}
/**
  * @brief Nixie tube scanning function
           Suggest putting it in timer interrupts
  */
void num_showScan(void)
{
	static uint8_t num;
	P0_PORT = 0XFF;
    ENABLE_NUMLED_RB = 1;                     
    P2_PORT &= ~(7<<0); 
    P2_PORT |= (num%7<<0);
	P0_PORT = NUM_BUFF[(num++) % 7];   
	ENABLE_NUMLED_RB = 0;                     
}
/**
  * @brief  Digital display function
  * @param[in] longnum: Data to display
  * @bug Floating point or character is not supported
  */
void num_showNum(uint32_t longnum)
{ 
	uint8_t n = 6;                      
    while (n)
	{
        NUM_BUFF[n - 1] = NUM_ARRAY[longnum % 10];
        longnum /= 10;
        n--;  
        if (longnum == 0)
        {
            NUM_BUFF[n - 1] = 0xff;///<Remove the previous bit
            break;
        }           
    }
    NUM_BUFF[6] = P0_BUFF;
}
/**
  * @brief  Digital Tube Displays Floating Point Data
  * @param[in] floatnum: Data to display
  * @param[in] num_digitDecimal: Number of digits to display data 
  * @bug none
  */
void num_showFloatNum(float floatnum,int16_t num_digitDecimal)
{
    uint8_t n = 6;
    floatnum = floatnum *pow(10,num_digitDecimal);///<Let reserved decimals become integer parts
    while (n)
    {
        NUM_BUFF[n-1] = NUM_ARRAY[(int32_t)floatnum % 10];
        floatnum /= 10.0;
        n--;
        if ((int32_t)floatnum == 0)
        {
            NUM_BUFF[5 - num_digitDecimal] &= 0x7f;///<Add decimal points
            NUM_BUFF[n - 1] = 0xff;///<Remove the previous bit
            break;
        }
    }
    NUM_BUFF[6] = P0_BUFF;
}
/**
  * @brief Calculate the Y power of X 
  * @param[in] multiplier_x: Base number
  * @param[in] multiplier_y: power 
  * @bug none
  */
static uint32_t pow(int16_t multiplier_x,int16_t multiplier_y)
{
    uint8_t i;
    uint32_t sum = 1;
    for (i = 0; i < multiplier_y; i++)
    {
        sum *= multiplier_x;
    }
    return sum;
}
/**
  * @}
  */
/************************ (C) COPYRIGHT WS  *****END OF FILE*********************************/
