/**
  *### Copyright (c) 2018, EDD-STC Development Team (eddstc1984@gmail.com). ######
  *### SPDX-License-Identifier: Apache-2.0 ######
  ********************************************************************************************
  * @file drv_uart.h
  * @version 0.1.0	
  ********************************************************************************************
  *###Change Logs:
  *     Date        |   Author      |   Notes
  * ----------------|---------------|---------------------------------------------------------
  *     2018-11-23  |   GYC         |   the first version
***/

/* Define to prevent recursive inclusion ---------------------------------------------------*/
#ifndef __DRV_PWM_H__
#define __DRV_PWM_H__

#ifdef __cplusplus
 extern "C" {
#endif

/* Includes --------------------------------------------------------------------------------*/     
#include "board.h" 
#include <stdio.h>     
/* Exported types --------------------------------------------------------------------------*/
/* Exported constants ----------------------------------------------------------------------*/
/* Exported macro --------------------------------------------------------------------------*/ 	
/* Exported functions ----------------------------------------------------------------------*/ 
void pwm_init(uint16_t per);
void pwm_set(uint16_t fre);    
#ifdef __cplusplus
}
#endif