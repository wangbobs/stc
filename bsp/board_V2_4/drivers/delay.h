/**
  *### Copyright (c) 2018, EDD-STC Development Team (eddstc1984@gmail.com). ######
  *### SPDX-License-Identifier: Apache-2.0 ######
  ********************************************************************************************
  * @file delay.h
  * @version 0.1.0	
  ********************************************************************************************
  *###Change Logs:
  *     Date        |   Author      |   Notes
  * ----------------|---------------|---------------------------------------------------------
  *     2019-2-15   |   WS          |   the first version
***/

/* Define to prevent recursive inclusion ---------------------------------------------------*/
#ifndef __DELAY_H__
#define __DELAY_H__

#ifdef __cplusplus
 extern "C" {
#endif

/* Includes --------------------------------------------------------------------------------*/   
#include "board.h" 
#include <stdio.h>  
#include <intrins.h>      
/* Exported types --------------------------------------------------------------------------*/
/* Exported constants ----------------------------------------------------------------------*/
extern void _nop_(void);
/* Exported macro --------------------------------------------------------------------------*/ 	
/* Exported functions ----------------------------------------------------------------------*/ 
void delay_us(uint32_t n);
void delay_ms(uint32_t n);     
#ifdef __cplusplus
}
#endif
/**
  * @}
  */
/************************ (C) COPYRIGHT WS*****END OF FILE*********************************/