/**
  *### Copyright (c) 2018, EDD-STC Development Team (eddstc1984@gmail.com). ######
  *### SPDX-License-Identifier: Apache-2.0 ######
  ********************************************************************************************
  * @file delay.c
  * @version 0.1.0 
  * @brief none
  * @details none
  * @warning 
  * @bug none  
  ********************************************************************************************
  *###Change Logs:
  *     Date        |   Author      |   Notes
  * ----------------|---------------|---------------------------------------------------------
  *     2019-2-15   |   WS          |   the first version
***/


/* Includes --------------------------------------------------------------------------------*/
#include "delay.h"
/** @addtogroup STC_BSP
  * @{
  */
/* Private typedef -------------------------------------------------------------------------*/
/* Private define --------------------------------------------------------------------------*/
/* Private macro ---------------------------------------------------------------------------*/
/* Private variables -----------------------------------------------------------------------*/

/* Private function prototypes -------------------------------------------------------------*/
/* Exported functions ----------------------------------------------------------------------*/
# pragma optimize(4, SPEED)
///Delay n microseconds
void delay_us(uint16_t n)
{
    extern void _nop_(void);
    register unsigned char i = n, j = (n>>8);
    _nop_();
    _nop_(); 
    _nop_();
    if ((--i) | j)
    {
        do
        {
            _nop_();
            _nop_();
            _nop_();
            _nop_();
            _nop_();
            _nop_();
            _nop_();
            _nop_();
            _nop_();
            if (0xFF == (i--))
                j--;
            else 
                {
                _nop_();
                _nop_(); 
                _nop_();
                _nop_();
                _nop_();
                _nop_();
                _nop_();
                }
        } while (i | j);
    }
}
///Delay n milliseconds
void delay_ms(uint32_t n) 
{
    while (n--) 
    delay_us(1000);
}
/**
  * @}
  */
/************************ (C) COPYRIGHT WS *****END OF FILE*********************************/