/**
  *### Copyright (c) 2018, EDD-STC Development Team (eddstc1984@gmail.com). ######
  *### SPDX-License-Identifier: Apache-2.0 ######
  ********************************************************************************************
  * @file demo_num_led.c
  * @version 0.1.0 
  * @brief none
  * @details none
  * @warning none
  * @bug none  
  ********************************************************************************************
  *###Change Logs:
  *     Date        |   Author      |   Notes
  * ----------------|---------------|---------------------------------------------------------
  *     2018-2-23   |   WS          |   the first version
***/

/* Includes --------------------------------------------------------------------------------*/
#include "board.h"
#include "drv_num_led.h"
#include "drv_led.h"
#include "timer.h"
/** @addtogroup STC_APP
  * @{
  */
/* Private typedef -------------------------------------------------------------------------*/
/* Private define --------------------------------------------------------------------------*/
/* Private macro ---------------------------------------------------------------------------*/
/* Private variables -----------------------------------------------------------------------*/
/* Private function prototypes -------------------------------------------------------------*/
/* Exported functions ----------------------------------------------------------------------*/

void main(void)
{   
    uint16_t num = 1019;
    float floatnum = 100.9;
    numled_init();
    timer0_init();
    while (1)
        {
        if (num_led_scanFlag == 1)
            {
                num_led_scanFlag = 0;
                num_showScan();
             }
        if (led_runFlag == 1)
            {
                led_runFlag = 0;
                led_run();
//              num_showNum(num--);
                num_showFloatNum(floatnum,2); 
             }           
         }
}

/**
  * @}
  */
/************************ (C) COPYRIGHT GYC *****END OF FILE*********************************/